% # vim:set sw=4 ts=4 sts=4 ft=javascript expandtab:
%= javascript begin
    function link(url, dl, token, modify) {
        if (token !== undefined) {
            if (modify !== undefined && modify === true) {
                return  '<%== url_for('/m/')->to_abs() %>'+url+'/'+token;
            } else {
                url = 'd/'+url+'/'+token;
            }
        } else if (dl !== '') {
            url = url+'?'+dl;
        }
        return '<a href="<%== url_for('/')->to_abs() %>'+url+'"><%== url_for('/')->to_abs() %>'+url+'</a>';
    }
    function share(url) {
        console.log(url);
        new MozActivity({
            name: "share",
            data: {
                type: "url",
                number: 1,
                url: url
            }
        });
    }
    function tw_url(url) {
        var btn = '&nbsp;&nbsp;&nbsp;<a title="<%= l('Tweet it!') %>" target="_blank" href="https://twitter.com/share?url=<%== url_for('/')->to_abs() %>'+url+'?t"><span class="icon icon-twitter"></span></a>';
        if (navigator.mozSetMessageHandler !== undefined) {
            btn = btn+'&nbsp;&nbsp;&nbsp;<a title="<%= l('Share it!') %>" target="_blank" href="" onclick="share(\'<%== url_for('/')->to_abs() %>'+url+'?t\');return false;"><span class="icon icon-share"></span></a>';
        }
        return btn
    }
    function modify(url, short) {
        $.ajax({
            url  : url,
            type : "POST",
            data : {
                'image_url'      : '<%== url_for('/')->to_abs() %>'+short,
                'format'         : 'json',
                'first-view'     : ($("#first-view-"+short).prop('checked')) ? 1 : 0,
                'delete-day'     : $("#day-"+short).val()
            },
            success: function(data) {
                alert(data.msg);
            },
            error: function() {
                alert('<%= l('Error while trying to modify the image.') %>');
            }
        });
    }
    function build_message(success, msg) {
        if(success) {
            var thumb = (msg.thumb !== null) ? '<img class="pull-left thumbnail" alt="'+msg.filename+' thumbnail" src="'+msg.thumb+'">' : ''
            return '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
                +thumb
                +'<div><strong>'
                +msg.filename
                +'</strong>'
                +tw_url(msg.short)
                +'<ul class="list-unstyled"><li><i class="icon icon-eye" title="<%= l('View link') %>"></i>&nbsp;'
                +link(msg.short, '')
                +'</li><li><i class="markdown-mark-solid" title="<%= l('Markdown syntax') %>"></i>&nbsp;![](<%== url_for('/')->to_abs() %>'+msg.short+')'
                +'</li><li><i class="icon icon-download" title="<%= l('Download link') %>"></i>&nbsp;'
                +link(msg.short, 'dl')
                +'</li><li><i class="icon icon-share" title="<%= l('Link for share on social networks') %>"></i>&nbsp;'
                +link(msg.short, 't')
                +'</li><li><i class="icon icon-trash" title="<%= l('Deletion link') %>"></i>&nbsp;'
                +link(msg.real_short, '', msg.token)
                +'</li></ul><form class="form" role="form" method="POST" action="'
                +link(msg.real_short, '', msg.token, true)
                +'"><div class="form-group form-inline"><select id="day-'+msg.real_short+'" name="delete-day" class="form-control">'
% for my $delay (qw/0 1 7 30 365/) {
%   my $text = ($delay == 7 || $delay == 30) ? l('%1 days', $delay) : $d->{'delay_'.$delay};
%   if (config('max_delay')) {
%       if ($delay) {
%           if ($delay < config('max_delay')) {
                +'<option value="<%= $delay %>" <%== is_selected($delay) %>><%= $text %></option>'
%           } elsif ($delay == config('max_delay')) {
                +'<option value="<%= $delay %>" <%== is_selected($delay) %>><%= $text %></option>'
%               last;
%           } else {
%               my $text = ($delay == 1) ? l('24 hours') : l('%1 days', $delay);
                +'<option value="<%= config('max_delay') %>" <%== is_selected(config('max_delay')) %>><%= l('%1 days', config('max_delay')) %></option>'
%               last;
%           }
%       }
%   } else {
                +'<option value="<%= $delay %>" <%== is_selected($delay) %>><%= $text %></option>'
%   }
% }
                +'</select>&nbsp;<div class="checkbox"><label><input id="first-view-'+msg.real_short+'" type="checkbox" name="first-view"> <%= l('Delete at first view?') %></label>'
                +'</div>&nbsp;'
                +'<a href="#" onclick="modify(\''+link(msg.real_short, '', msg.token, true)+'\', \''+msg.real_short+'\');return false;" class="btn btn-sm btn-default btn-primary"><%= l('Let\'s go!') %></a></div></form>'
                +'</div>';
        } else {
            return '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong><%= l('Something bad happened') %></strong><br>'
                +msg.filename
                +"<br>"
                +msg.msg
                +'</div>';
        }
    }
    function bindddz(firstview, deleteday) {
        $('#drag-and-drop-zone').dmUploader({
            url: '<%== url_for('/') %>',
            dataType: 'json',
            allowedTypes: 'image/*',
            maxFileSize: <%= $max_file_size %>,
            onNewFile: function(id, file){
                $(".messages").append('<div id="'+id+'-div">'+file.name+'<br><div class="progress"><div id="'+id+'"class="progress-bar progress-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span id="'+id+'-text" class="pull-left" style="padding-left: 10px;"> 0%</span></div></div></div>');
            },
            onUploadProgress: function(id, percent){
                var percentStr = ' '+percent+'%';
                $('#'+id).prop('aria-valuenow', percent);
                $('#'+id).prop('style', 'width: '+percent+'%;');
                $('#'+id+'-text').html(percentStr);
            },
            onUploadSuccess: function(id, data){
                $('#'+id+'-div').remove();
                $(".messages").append(build_message(data.success, data.msg));
            },
            onUploadError: function(id, message){
                $(".messages").append(build_message(false, ''));
            },
            onFileSizeError: function(file){
                $(".messages").append(build_message(false, { filename: file.name, msg: '<%= l('The file exceed the size limit (%1)', $max_file_size) %>'}));
            }
        });
    }

    function upload_url() {
        var val = $("#lutim-file-url").val();
        if (val !== undefined && val !== "") {
            $("#lutim-file-url").prop('disabled', 'disabled');
            $(".hidden-spin").css('display', 'block');
            console.log(val);
            $.ajax({
                url  : '<%== url_for('/') %>',
                type : "POST",
                data : {
                    'lutim-file-url' : val,
                    'format'         : 'json',
                    'first-view'     : ($("#first-view").prop('checked')) ? 1 : 0,
                    'crypt'          : ($("#crypt").prop('checked')) ? 1 : 0,
                    'delete-day'     : $("#delete-day").val()
                },
                success: function(data) {
                    $(".messages").append(build_message(data.success, data.msg));
                    if (data.success) {
                        $("#lutim-file-url").val('');
                    }
                },
                error: function() {
                    $(".messages").append(build_message(false, ''));
                },
                complete: function() {
                    $("#lutim-file-url").prop('disabled', '');
                    $(".hidden-spin").css('display', 'none');
                }
            });
        } else {
            console.log("fhdsjnf");
        }
    }

    function fileUpload(file) {
        var fd = new FormData();
        fd.append('file', file);

        fd.append('format', 'json');
        fd.append('first-view', ($("#first-view").prop('checked')) ? 1 : 0);
        fd.append('crypt', ($("#crypt").prop('checked')) ? 1 : 0);
        fd.append('delete-day', ($("#delete-day").val()));

        $(".messages").append('<div id="1-div">'+file.name+'<br><div class="progress"><div id="1"class="progress-bar progress-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span id="1-text" class="pull-left" style="padding-left: 10px;"> 0%</span></div></div></div>');
        // Ajax Submit
        $.ajax({
            url: '<%== url_for('/') %>',
            type: 'POST',
            dataType: 'json',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            forceSync: false,
            xhr: function(){
                var xhrobj = $.ajaxSettings.xhr();
                if(xhrobj.upload){
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total || e.totalSize;
                        if(event.lengthComputable){
                          percent = Math.ceil(position / total * 100);
                        }

                        var percentStr = ' '+percent+'%';
                        $('#1').prop('aria-valuenow', percent);
                        $('#1').prop('style', 'width: '+percent+'%;');
                        $('#1-text').html(percentStr);
                    }, false);
                }

                return xhrobj;
            },
            success: function (data, message, xhr){
                $('#1-div').remove();
                $(".messages").append(build_message(data.success, data.msg));
            },
            error: function (xhr, status, errMsg){
                $(".messages").append(build_message(false, ''));
            },
        });
    }
    window.onload = function() {
        if (navigator.mozSetMessageHandler !== undefined) {
            navigator.mozSetMessageHandler('activity', function handler(activityRequest) {
                var activityName = activityRequest.source.name;
                if (activityName == 'share') {
                    activity = activityRequest;
                    blob = activity.source.data.blobs[0];
                    fileUpload(blob);
                }
            });
        }
    };
    $('document').ready(function() {
        var firstview = ($("#first-view").prop('checked')) ? 1 : 0;
        var deleteday = ($("#delete-day").prop('checked')) ? 1 : 0;

        bindddz(firstview, deleteday);

        $("#file-url-button").on("click", upload_url);
        $('#lutim-file-url').keydown( function(e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            if(key == 13) {
                e.preventDefault();
                upload_url();
            }
        });
    });
% end
