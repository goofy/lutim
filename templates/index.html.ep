% # vim:set sw=4 ts=4 sts=4 ft=html.epl expandtab:
% my %d = (
%   delay_0   => l('no time limit'),
%   delay_1   => l('24 hours'),
%   delay_365 => l('1 year')
% );
<div class="messages">
% if (config('always_encrypt')) {
    <p><%= l('The images are encrypted on the server (Lutim does not keep the key).') %></p>
% }
% if (defined(stash('short'))) {
    <div class="alert alert-success">
%     if (defined(stash('short'))) {
        <img class="pull-left thumbnail" alt="<%= stash('filename') %> thumbnail" src="<%= stash('thumb') %>">
%     }
        <div>
% # Display image informations
            % my $url = url_for('/'.stash('short'))->to_abs();
            <strong><%= stash('filename') %></strong>
            &nbsp;&nbsp;&nbsp;<a title="<%= l('Tweet it!') %>" target="_blank" href="https://twitter.com/share?url=<%= $url %>?t"><span class="icon icon-twitter"></span></a>
            <ul class="list-unstyled">
                % my $delete_url = url_for('delete', {short => stash('real_short'), token => stash('token')})->to_abs();
                <li><i class="icon icon-eye"       title =" <%= l('View link') %>"></i>                         <%= link_to $url        => begin %> <%= $url %>        <%= end %></li>
                <li><i class="markdown-mark-solid" title =" <%= l('Markdown syntax') %>"></i>                   ![](<%= $url %>)</li>
                <li><i class="icon icon-download"  title =" <%= l('Download link') %>"></i>                     <%= link_to $url.'?dl'  => begin %> <%= $url.'?dl' %>  <%= end %></li>
                <li><i class="icon icon-share"     title =" <%= l('Link for share on social networks') %>"></i> <%= link_to $url.'?t'   => begin %> <%= $url.'?t' %>   <%= end %></li>
                <li><i class="icon icon-trash"     title =" <%= l('Deletion link') %>"></i>                     <%= link_to $delete_url => begin %> <%= $delete_url %> <%= end %></li>
            </ul>
        </div>
% # Delay modification form
        % my $modify_url = url_for('modify', {short => stash('real_short'), token => stash('token')})->to_abs();
        <form class="form" role="form" method="POST" action="<%== $modify_url %>">
            <div class="form-group form-inline">
                <input name="image_url" type="hidden" value="<%= $url %>">
                <select name="delete-day" class="form-control">
                    %= include 'partial/for_my_delay', d => \%d
                </select>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="first-view"> <%= l('Delete at first view?') %>
                    </label>
                </div>
                <%= submit_button l('Let\'s go!'), class => 'btn btn-sm btn-default btn-primary', id => 'submitbutton' %>
            </div>
        </form>
    </div>
% }
% if (defined(flash('success'))) {
    <div class="alert alert-success">
        <button type="button" class="close jsonly" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p><%== flash('success') %></p>
    </div>
% }
% if (defined(flash('msg'))) {
    <div class="alert alert-danger">
        <button type="button" class="close jsonly" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong><%= l('Something bad happened') %></strong><br>
        <%= flash('filename') %> <%= flash('msg') %>
    </div>
% }
</div>

<noscript>
    <form class="form" role="form" method="POST" action="<%= url_for('/') %>" enctype="multipart/form-data">
        <div class="form-group form-inline">
            <select name="delete-day" class="form-control">
                %= include 'partial/for_my_delay', d => \%d
            </select>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="first-view"> <%= l('Delete at first view?') %>
                </label>
                <label <%== (config('always_encrypt')) ? 'class="always-encrypt"' : '' %>>
                    <input type="checkbox" name="crypt"> <%= l('Encrypt the image (Lutim does not keep the key).') %>
                </label>
                <label>
                    <input type="checkbox" name="keep-exif"> <%= l('Keep EXIF tags') %>
                <label>
            </div>
        </div>
        <div class="form-group">
            <label for="lutim-file"><%= l('Send an image') %></label>
            <input type="file" name="file" id="lutim-file" accept="image/*">
        </div>
        <div class="form-group">
            <label for="lutim-file-url"><%= l('Upload an image with its URL') %></label>
            <input type="url" name="lutim-file-url" placeholder="<%= l('Image URL') %>">
        </div>
        <p class="help-block"><%= l('Only images are allowed') %></p>
        <%= submit_button l('Let\'s go!'), class => 'btn btn-default btn-primary', id => 'submitbutton' %>
    </form>
</noscript>

<!-- D&D Zone-->
<div class="jsonly">
    <div class="form-group form-inline">
        <select id="delete-day" class="form-control">
            %= include 'partial/for_my_delay', d => \%d
        </select>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="first-view"> <%= l('Delete at first view?') %>
            </label>
            <label <%== (config('always_encrypt')) ? 'class="always-encrypt"' : '' %>>
                <input type="checkbox" id="crypt"> <%= l('Encrypt the image (Lutim does not keep the key).') %>
            </label>
            <label>
                <input type="checkbox" id="keep-exif"> <%= l('Keep EXIF tags') %>
            <label>
        </div>
    </div>
    <div id="drag-and-drop-zone" class="uploader">
        <div><%= l('Drag & drop images here') %></div>
        <div class="or"><%= l('-or-') %></div>
        <div class="browser">
            <label>
                <span><%= l('Click to open the file browser') %></span>
                <input type="file" name="files[]" multiple="multiple" title='<%= l('Click to open the file browser') %>' accept="image/*">
            </label>
        </div>
    </div>
    <p class="help-block"><%= l('Only images are allowed') %></p>
    <form class="form-horizontal" role="form" method="POST" action="<%== url_for('add') %>">
        <div class="form-group">
            <span class="col-sm-3 col-xs-12"><span class="hidden-spin" style="font-size:200%; display:none;" > <i class="icon-spinner animate-spin pull-right"></i></span><label for="lutim-file-url" class="control-label pull-right"><%= l('Upload an image with its URL') %></label></span>
            <div class="col-sm-9 col-xs-12">
                <input type="url" name="lutim-file-url" class="form-control" id="lutim-file-url" placeholder="<%= l('Image URL') %>">
            </div>
        </div>
        <a href="#" class="btn btn-default btn-primary pull-right" id="file-url-button"><%= l('Let\'s go!') %></a>
    </form>
</div>
<!-- /D&D Zone -->

%= include 'partial/lutim', format => 'js', d => \%d
